var React = require('react');
var ReactDOM = require('react-dom');
var Reflux = require('reflux');
var Router = require('react-router');
var TransitionAction = require('./Actions/TransitionAction');

/**
 * Platform: React Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

var Platform, config = {
    cacheable: [],
    ga: false,
    named: [],
    renderMode: 'browser',
    routerCallback: undefined,
    scroll: true
};

module.exports = Platform = {

    // Make component cacheable
    cacheable: function() {

        // Skip if we are rendering on browser
        if (this.getRenderMode() !== 'server') {
            return;
        }

        // Create function to store cacheable source
        // when component is mounting
        return {
            componentWillMount: function() {
                var source = Platform.parseSource(this);

                // Add to cacheable array
                if (config.cacheable.indexOf(source) === -1) {
                    config.cacheable.push(source);
                }
            }
        };
    },

    // Connect to server state
    connect: function(source, checkCache) {
        var connect = Reflux.connect(require('./Stores/ServerStateStore'));

        // If we have been passed a source, transform into a
        // getSource function
        if (source !== undefined) {
            connect.getSource = function() {
                return trimSource(source);
            };
        }

        // Create check cache function
        if (checkCache !== undefined) {
            connect.checkCache = function() {
                return checkCache;
            };
        }

        // Override initial state to pass in source
        connect.getInitialState = function() {
            var checkCache = true, state;

            // If we have a check cache funtion, use this
            // to determine whether cache should be used
            if (this.checkCache !== undefined) {
                checkCache = this.checkCache(
                    Platform.getUri(),
                    config.router.getCurrentParams(),
                    config.router.getCurrentQuery()
                );
            }

            // Retrieve state from server state store
            state = require('./Stores/ServerStateStore').getInitialState(
                Platform.parseSource(this),
                checkCache
            );

            // Set title and return
            setTitle(state);
            return state;
        };

        // Set title on data load
        connect.componentDidUpdate = function() {
            setTitle(this.state);
        };

        return connect;
    },

    // Disable scroll
    disableScroll: function() {
        config.scroll = false;
    },

    // Return asset base
    getAssetBase: function() {
        return config.assetBase || '';
    },

    // Return cachable sources
    getCacheableSources: function() {
        var cacheable = config.cacheable;
        config.cacheable = [];

        return cacheable;
    },

    // Return asset base
    getMediaBase: function() {
        return config.mediaBase || '';
    },

    // Return previous path
    getPrevPath: function() {
        return config.prevPath;
    },

    // Return render mode
    getRenderMode: function() {
        return config.renderMode;
    },

    // Return router
    getRouter: function() {
        return config.router;
    },

    // Return routes
    getRoutes: function() {
        return config.routes;
    },

    // Return request URI
    getUri: function() {
        var uri = config.uri;

        // If we have no URI, use window
        if (uri === undefined) {
            uri = window.location.pathname + window.location.search;
        }

        return trimSource(uri);
    },

    // Parse component source
    parseSource: function(component) {
        var source = this.getUri();
        source = trimSource(source);

        // Retrieve source
        if (typeof component.getSource === 'function') {
            source = component.getSource(
                source,
                config.router.getCurrentParams(),
                config.router.getCurrentQuery()
            );
        }

        return trimSource(source);
    },

    // Mount app at passed dom element and run router
    run: function() {

        // If browser does not support push-state or we
        // are already running, prevent running of app
        if (config.running || !hasPushState()) {
            return;
        }

        Router.run(config.routes, Router.HistoryLocation, function(Handler, state) {
            Platform.setRouter(this);
            config.running = true;

            // Move to top of window
            if (config.scroll === true) {
                window.scroll(0, 0);
            }

            config.scroll = true;

            // Render route
            var element = React.createElement(Handler, null);
            ReactDOM.render(element, config.mount);
            TransitionAction();

            config.prevPath = state.path;

            // Update google analytics
            if (config.ga === true && window.ga !== undefined) {
                window.ga('set', 'page', window.location.pathname + window.location.search);
                window.ga('send', 'pageview');
            }

            // Enable GA
            config.ga = true;

            // Trigger any registered callback
            if (config.routerCallback !== undefined) {
                config.routerCallback();
            }
        });
    },

    // Set asset base
    setAssetBase: function(base) {
        if (base) {
            config.assetBase = base.replace(/\/+$/, '') + '/';
        }

        return this;
    },

    // Set media base
    setMediaBase: function(base) {
        if (base) {
            config.mediaBase = base.replace(/\/+$/, '') + '/';
        }

        return this;
    },

    // Set mounting element
    setMount: function(mount) {
        config.mount = mount;

        return this;
    },

    // Set named routes
    setNamedRoutes: function(named) {
        config.named = named;

        return this;
    },

    // Set render mode
    setRenderMode: function(mode) {
        config.renderMode = mode;

        return this;
    },

    // Set current router
    setRouter: function(router) {
        config.router = router;

        // Override make path function to check for any named routes
        // before calling checking actual routes.
        if (router._makePath === undefined) {
            router._makePath = router.makePath;

            router.makePath = function(to, params, query) {
                var i, length = config.named.length,
                    named;

                // Check if any named routes match
                for (i = 0; i < length; i++) {
                    named = config.named[i];

                    if (named.name === to) {
                        to = '/' + named.uri;
                        break;
                    }
                }

                // Call original function
                return this._makePath(to, params, query);
            };
        }

        return this;
    },

    // Set router callback
    setRouterCallback: function(callback) {
        config.routerCallback = callback;

        return this;
    },

    // Set routes to use when mounting
    setRoutes: function(routes) {
        config.routes = routes;

        return this;
    },

    // Set state
    setState: function(state) {
        require('./Stores/ServerStateStore').setState(state);

        return this;
    },

    // Set request URI
    setUri: function(uri) {
        config.uri = trimSource(uri);

        return this;
    },

    // Subscribe to state endpoint, refeshing
    // data at the passed interval
    subscribe: function(interval) {
        interval = interval || 5;
        var handle;

        return {

            // On mount, create interval for updating
            // state data
            componentDidMount: function() {
                handle = setInterval(function() {
                    require('./Stores/ServerStateStore').refreshState(
                        Platform.parseSource(this)
                    );
                }.bind(this), interval * 1000);
            },

            // On unmount, remove interval
            componentWillUnmount: function() {
                clearInterval(handle);
            }
        }
    }
};

// Return whether browser has push state support
var hasPushState = function() {
    return window.history && window.history.pushState;
};

// Set title
var setTitle = function(state) {
    var header;

    // Skip if we are rendering on server
    if (Platform.getRenderMode() === 'server') {
        return;
    }

    state = state || {};
    header = state.meta || {};
    header = header.header || {};

    // If title exists and is different, update
    if (typeof header.title === 'string') {

        // Is it different to current title?
        if (header.title !== config.title) {
            document.title = config.title = header.title;
        }
    }
};

// Trim source
var trimSource = function(source) {
    if (source === undefined) {
        return undefined;
    }

    return source.replace(/^\/+|\/+$/g, '');
};