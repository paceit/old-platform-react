var Platform         = require('platform-react');
var React            = require('react');
var Reflux           = require('reflux');
var Router           = require('react-router');
var TransitionAction = require('../Actions/TransitionAction');
var xhr              = require('xhr');

/**
 * Platform: React Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

var cache       = {};
var components  = {};
var initialLoad = true;
var missing     = false;
var requested   = [];
var requests    = {};
var uuidCounter = 0;

// Create store
module.exports = Reflux.createStore({

    // Listen to transition actions
    init: function() {
        this.listenTo(TransitionAction, this.onTransition);
    },
    
    // Get cache
    clearCache: function() {
        cache       = {};
        components  = {};
        missing     = false;
        requested   = [];
        requests    = {};
        uuidCounter = 0;
    },

    // Return cached sever state to component. Cache will generally only
    // be available if we are preloading or have rendered a component
    // using the state previously.
    getInitialState: function(source, useCache) {
        source = trimSource(source || Platform.getUri());

        // If rendering on server, add to requested sources
        if (Platform.getRenderMode() === 'server') {
            if (requested.indexOf(source) === -1) {
                requested.push(source);
            }
        }

        // If this is the initial load, override to always use cache
        if (initialLoad === true) {
            useCache = true;
        }
        
        // Return from cache
        if (useCache !== false && cache[source] !== undefined) {
            return cache[source];
        }

        // If we are rendering on client, run refresh state.
        // This stops us from having to trigger transition
        // actions on components that mount after the
        // initial transition
        if (Platform.getRenderMode() !== 'server') {
            this.refreshState(source);
        }

        // Flag missing state
        missing = true;
        
        return {};
    },
    
    // Return requested sources
    getRequestedSources: function() {
        return requested;
    },

    // Return missing data status
    missingData: function() {
        return missing;
    },

    // Preload state for passed source or sources
    preloadInitialState: function(sources) {
        var i, length, source;

        // Make sure we are processing an array
        sources = [].concat(sources);
        length  = sources.length;

        // Retrieve state from server
        for (i = 0; i < length; i++) {
            source = trimSource(sources[i]);

            // If rendering on server, add to requested sources
            if (Platform.getRenderMode() === 'server') {
                if (requested.indexOf(source) === -1) {
                    requested.push(source);
                }
            }

            // If we already have cache, skip
            if (cache[source] !== undefined) {
                continue;
            }

            // If we are rendering on client, load source
            if (Platform.getRenderMode() !== 'server') {
                loadSource(source);
            }

            // Flag missing state
            missing = true;
        }
    },

    // When a transition event is triggered, retrieve server state
    // and pass to any listeners
    onTransition: function() {
        var me = this, parsed = [], source, uuid;

        // If no-one is listening for state, no need to process
        if (this.emitter.listeners('change').length === 0) {
            return;
        }

        // Loop through listening components
        for (uuid in components) {
            if (! components.hasOwnProperty(uuid)) {
                continue;
            }

            // Parse source
            source = parseSource(uuid);
            if (source === false) {
                continue;
            }

            // Don't process the same source multiple times
            if (parsed.indexOf(source) !== -1) {
                continue;
            }

            parsed.push(source);

            // Do we have cached state?
            if (checkCache(uuid) !== false && cache[source] !== undefined) {
                me.trigger(cache[source], source);
                continue;
            }

            // Load source
            (function(source) {
                var timeout;

                // Create timeout for showing loading indicator. If retrieving
                // state from the server takes longer than 150ms, components
                // listening to source will recieve loading state.
                timeout = setTimeout(function () {
                    me.trigger({loading: true}, source);
                }, 150);

                // Retrieve state from server, then run transition method to
                // update components listening to source
                loadSource(source, function(data) {
                    clearTimeout(timeout);
                    me.trigger(data, source);
                });

            })(source);
        }

        // Disable initial load
        initialLoad = false;
    },

    // Refresh state
    refreshState: function(sources) {
        var i, length, me = this, source;

        // Make sure we are processing an array
        sources = [].concat(sources);
        length  = sources.length;

        // Retrieve state from server
        for (i = 0; i < length; i++) {
            source = trimSource(sources[i]);

            // Load source
            (function(source) {
                loadSource(source, function(data) {
                    me.trigger(data, source);
                });
            })(source);
        }
    },

    // Set state
    setState: function(source, state, trigger) {
        var key, newState, parsedSource;

        // If we have been passed two arguments, transform
        // to state object
        if (state !== undefined) {
            newState         = {};
            newState[source] = state;
            state            = newState;
        } else {
            state = source;
        }

        // Loop through state
        for (source in state) {
            if (! state.hasOwnProperty(source)) {
                continue;
            }

            // Add state to cache
            parsedSource        = trimSource(source);
            cache[parsedSource] = state[source];

            // Trigger update
            if (trigger !== false) {
                this.trigger(cache[parsedSource], parsedSource);
            }
        }
    }
});

module.exports.listen = function(callback, bindContext) {
    var aborted = false, func, me = this, stateA, stateB, uuid = uuidCounter++;

    bindContext      = bindContext || this;
    components[uuid] = bindContext;

    // Build event handler
    var eventHandler = function(args) {
        var aborted = false, source, useCache;

        if (aborted) {
            return;
        }

        // If this is cache, check whether component is using cache
        if (args[2] === true && checkCache(uuid) === false) {
            return;
        }

        // If source we are handling does not match passed, return
        source = parseSource(uuid);
        if (source !== args[1]) {
            return;
        }

        // Compare state, it nothing has changed don't trigger
        stateA = JSON.stringify(components[uuid].state);
        stateB = JSON.stringify(args[0]);
        if (stateA === stateB) {
            return;
        }

        callback.apply(bindContext, [args[0]]);
    };

    // Listen for event
    this.emitter.addListener(this.eventLabel, eventHandler);

    // Return remove listener function
    return function() {
        me.emitter.removeListener(me.eventLabel, eventHandler);

        delete components[uuid];
        aborted = true;
    };
};

// Check cache
var checkCache = function(uuid) {
    var component = components[uuid];

    // If we can't find component or this is
    // this initial load, return true
    if (initialLoad === true || component === undefined) {
        return true;
    }

    // Run check cache function
    if (component.checkCache !== undefined) {
        return component.checkCache(
            Platform.getUri(),
            Platform.getRouter().getCurrentParams(),
            Platform.getRouter().getCurrentQuery()
        );
    }

    return true;
};

// Load source and run callback
var loadSource = function(source, cb) {
    var xhrSource;

    // If we are already processing request, add to callbacks
    // and return
    if (requests[source]) {
        if (cb) {
            requests[source].push(cb);
        }

        return;
    }

    // Add react timestamp to source
    //
    // Prevents browser returning JSON response from cache rather
    // than rendering on server
    xhrSource  = source;
    xhrSource += source.indexOf('?') === -1 ? '?' : '&';
    xhrSource += 'react=' + new Date().getTime();

    // Store callback
    requests[source] = [];
    if (cb) {
        requests[source].push(cb);
    }

    // Submit AJAX request
    xhr({
        url: xhrSource,
        headers: {

            // Pass React version in headers
            'X-Powered-By': 'React ' + React.version
        }
    },

    // Handle request
    function(error, response, body) {
        var i, length;

        // Handle invalid response
        if (response.statusCode !== 200) {

            // If we get anything other than an interval server error,
            // refresh page. This will trigger any refreshes etc.
            if (response.statusCode !== 500) {
                window.location.reload();
            }

            return;
        }

        // Parse data
        cache[source] = JSON.parse(body);
        cache[source].loading = false;

        // Loop through callbacks
        length = requests[source].length;
        for (i = 0; i < length; i++) {
            requests[source][i](cache[source]);
        }

        delete requests[source];
    });
};

// Parse UUID source
var parseSource = function(uuid) {
    var component = components[uuid], source;

    // If we can't find source, return false
    if (component === undefined) {
        return false;
    }

    // Parse source function
    if (component.getSource) {
        source = component.getSource(
            Platform.getUri(),
            Platform.getRouter().getCurrentParams(),
            Platform.getRouter().getCurrentQuery()
        );
    }
    
    // If we have no get source function, use current URI
    else {
        source = Platform.getUri();
    }

    return trimSource(source);
};

// Trim source
var trimSource = function(source) {
    var index = source.indexOf('?'), query;

    // Reorder query string alphabetically. This allows
    // us to reuse valid cache that wouldn't be matched
    // otherwise.
    if (index !== -1) {
        query   = source.substr(index + 1);
        query   = query.split('&').sort();

        source  = source.substr(0, index) + '?';
        source += query.join('&');
    }

    // Decode in case string has encoded characters
    source = decodeURIComponent(source);

    return source.replace(/^\/+|\/+$/g, '');
};
