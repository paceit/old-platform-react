var cluster = require('cluster');
var os      = require('os');

/**
 * Platform: React Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// Socket
var socket = 8000;

module.exports = {

    // Set socket
    setSocket: function(newSocket) {
        socket = newSocket;
    },

    // Listen for render requests
    listen: function() {

        // If we are cluster master, create as many child
        // processes as we have CPU cores
        if (cluster.isMaster) {
            var i, processes = os.cpus().length;

            for (i = 0; i < processes; i++) {
                cluster.fork({
                    socket: socket
                });
            }

            cluster.on('exit', function() {
                cluster.fork({
                    socket: socket
                });
            });
        }

        // If we are child process, listen for render requests
        else {
            require('./socket');
        }
    }
};
