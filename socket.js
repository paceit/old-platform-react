var Platform = require('platform-react');
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var Router = require('react-router');
var ServerStateStore = require('./Stores/ServerStateStore');
var net = require('net');

/**
 * Platform: React Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// Support loading of JSX files
require('node-jsx').install();

// Listen for render requests
net.createServer(function(client) {
    var buffer = '',
        elem, length, newRequest = true,
        tries = 0, requestCount = 0;

    // If we are not in production, clear any cache
    if (process.env.NODE_ENV !== 'production') {
        clearCache();
    }

    // Handle incoming data
    client.on('data', function(data) {
        var cache = false,
            cacheable, index, markup, requested;

        try {
            data = data.toString();

            // First call is always length of incoming data
            if (length === undefined) {
                index = data.indexOf('|');
                length = data.substr(0, index);
                data = data.substr(index + 1);
            }

            buffer += data;

            // Return if length of buffer is less than expected
            if (buffer.length < length) {
                return;
            }

            data = JSON.parse(buffer);

            // Parse new connection
            if (newRequest === true) {

                // Setup new request
                Platform
                    .setAssetBase(data.assetBase)
                    .setMediaBase(data.mediaBase)
                    .setNamedRoutes(data.named)
                    .setRenderMode('server')
                    .setUri(data.uri);

                // Load routes
                Platform.setRoutes(
                    require(data.routes)
                );

                // Setup router
                Router.run(Platform.getRoutes(), '/' + data.uri, function(handler) {
                    Platform.setRouter(this);
                    elem = React.createElement(handler);
                });

                // Toggle new request flag
                newRequest = false;
            }

            // Store state
            ServerStateStore.clearCache();
            Platform.setState(data.state);

            // Render component and retrieve requested sources
            markup = ReactDOMServer.renderToString(elem);
            requested = ServerStateStore.getRequestedSources();
            cacheable = Platform.getCacheableSources();
            tries++;

            // Is the current request cacheable?
            if (cacheable.indexOf(Platform.getUri()) !== -1) {
                cache = true;
            }

            // Do we have missing data?
            if (tries < 5 && ServerStateStore.missingData() === true) {

                // If we are going to cache the current request,
                // we need to make sure any non-cacheable components
                // are not stored
                if (cache === true) {
                    requested = requested.filter(function(source) {
                        return cacheable.indexOf(source) !== -1;
                    });
                }

                // Write requested data back to client
                client.write('r||' + JSON.stringify(requested) + '||');

                // If data is missing client should be using same
                // connection so clear buffer and return so we don't
                // close the socket
                length = undefined;
                waiting = true;
                buffer = '';
                return;
            }

            // Return markup to client, starting response with
            // 'c' if the connectee can cache the markup
            client.write((cache ? 'c' : 'm') + '||' + markup + '||');

            // Return any exceptions
        } catch (e) {
            client.write('e||' + e.stack + '||');
        }

        client.end();

        // If we have processed more than 100 requests,
        // exit the process in case we have any memory
        // leaks. The cluster will restart the process
        // immediately.
        requestCount++;
        if (requestCount >= 100) {
            process.exit();
        }

    });

}).listen(process.env['socket']);

// Clear cache
var clearCache = function() {
    var count, file, i, module, modules = [];

    for (file in require.cache) {

        // Skip if file does not end in jsx extension
        if (file.indexOf('.jsx', file.length - 4) === -1) {
            continue;
        }

        modules.push(file);
        module = require.cache[file];

        // Clear parent module cache
        while (module && module.parent) {
            module = module.parent;
            modules.unshift(module.id);
        }

        // Delete cache
        count = modules.length;
        for (i = 0; i < count; i++) {
            delete require.cache[modules[i]];
        }
    }
};